set exrc
set nocompatible

filetype off
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Bundle 'gmarik/Vundle.vim'
Bundle 'wincent/Command-T.git'
Bundle 'terryma/vim-multiple-cursors.git'
Bundle 'scrooloose/nerdcommenter.git'
Bundle 'tpope/vim-surround.git'
Bundle 'SirVer/ultisnips.git'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'Valloric/YouCompleteMe.git'
Bundle 'jeaye/color_coded'
Bundle "bling/vim-airline.git"
Bundle "vim-scripts/TagHighlight.git"
Bundle 'altercation/vim-colors-solarized'
Bundle 'rdnetto/YCM-Generator'
Plugin 'flazz/vim-colorschemes'
Bundle 'powerline/powerline'
Bundle 'godlygeek/tabular'
Bundle 'reedes/vim-lexical'
Bundle 'mhinz/vim-startify'
Bundle 'shougo/unite.vim'
Plugin 'rizzatti/dash.vim'
Plugin 'vim-voom/VOoM'
Plugin 'lervag/vimtex'
Plugin 'rking/ag.vim'
Plugin 'rhysd/vim-grammarous'
Plugin 'scrooloose/nerdtree'
Plugin 'leafgarland/typescript-vim'
Plugin 'quramy/vim-js-pretty-template'
Plugin 'shougo/vimproc.vim'
Plugin 'quramy/tsuquyomi'
Plugin 'davidhalter/jedi-vim'
Plugin 'alepez/vim-gtest'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Shougo/deoplete.nvim'
call vundle#end()




syntax on
filetype plugin indent on
syntax spell toplevel


set background=dark
colorscheme molokaied

set confirm





augroup lexical
  autocmd!
  autocmd FileType markdown,mkd,tex call lexical#init()
  autocmd FileType textile call lexical#init()
  autocmd FileType text call lexical#init({ 'spell': 0 })
augroup END


let g:lexical#thesaurus = ['~/.vim/thesaurus/mthesaur.txt',]
let g:lexical#dictionary = ['/usr/share/dict/words',]

" set of options that are for all languages
inoremap jj <Esc>
map <S-Enter> O<Esc>
map <CR> o<Esc>

"" remapping the arrow keys
nnoremap j gj
nnoremap k gk


"" remapping the window movesment keys
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

map <C-Left> <Esc>:tabprev<CR>
map <C-Right> <Esc>:tabnext<CR>

set backspace=indent,eol,start

" setting the indentation options
set tabstop=4
set shiftwidth=4


set textwidth=120


set cursorline
"set linespace=5


"" Font / Colours Settings


set guifont=Monaco\ 10








:command W w
:command Wa wa
:command WA wa

set spelllang=en_gb


"" numbers and scrolling
:set nu
:set scrolloff=10

set wrap
set linebreak
set nolist 
set textwidth=0
set wrapmargin=0

""""""""""""""""""""""""""""""""""""""""""""""""""
"" Plugin Sepcific Options
""""""""""""""""""""""""""""""""""""""""""""""""""


" Ultisnips mappings
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsEditSplit='vertical'
let g:UltiSnipsSnippetsDir=$HOME.'/Uni/usnippets'
let g:UltiSnipsSnippetDirectories=["UltiSnips", $HOME.'/Uni/usnippets']
let g:ycm_python_binary_path = 'python'
let g:ycm_server_python_interpreter = '/usr/bin/python'
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_always_populate_location_list=1






"set rtp+=/Users/raguay/Library/Python/2.7/lib/python/site-packages/powerline/bindings/vim
python import sys; sys.path.append("/usr/local/lib/python2.7/site-packages")
set rtp+=/usr/local/lib/python2.7/site-packages/powerline/bindings/vim 
"" These lines setup the environment to show graphics and colors correctly.
set nocompatible
set t_Co=256



"python from powerline.vim import setup as powerline_setup
"python powerline_setup()
"python del powerline_setup
 
if ! has('gui_running')
	set ttimeoutlen=10
   	augroup FastEscape
      	autocmd!
     	au InsertEnter * set timeoutlen=0
      	au InsertLeave * set timeoutlen=1000
   	augroup END
endif
 
set laststatus=2 " Always display the statusline in all windows
"set guifont=Monaco\ for\ Powerline:h14
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)

syntax enable
set background=dark
set linespace=3

let g:CommandTTraverseSCM="pwd"

" Change default target to pdf, if not dvi is used
let g:Tex_DefaultTargetFormat = 'pdf'
"let g:Tex_AutoFolding = 0
"let g:Tex_MultipleCompileFormats = 'pdf, bibtex, pdf'
"" Setup the compile rule for pdf to use pdflatex with synctex enabled
"let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 --interaction=nonstopmode  $*' 
"let g:Tex_IgnoredWarnings =
			"\'Underfull'."\n".
			"\'Overfull'."\n".
			"\'specifier changed to'."\n".
			"\'You have requested'."\n".
			"\'Missing number, treated as zero.'."\n".
			"\'There were undefined references'."\n".
			"\'Latex Warning:'."\n".
			"\'Citation %.%# undefined'
"let g:Tex_IgnoreLevel = 8
"" PDF display rule
"let g:Tex_ViewRule_pdf = 'Skim'
"let g:vimtex_view_general_viewer
			"\ = '/Users/oliverferoze/Applications/Skim.app/Contents/SharedSupport/displayline'
let g:vimtex_view_general_options = '-r @line @pdf @tex'
"let g:vimtex_latexmk_continuous=0
"let g:vimtex_latexmk_background=1
let g:vimtex_index_split_pos='vert rightbelow'
let g:vimtex_index_split_width=120

"let g:vimtex_latexmk_callback_hooks = ['UpdateSkim']
"function! UpdateSkim(status)
	"if !a:status | return | endif

	"let l:out = b:vimtex.out()
	"let l:tex = expand('%:p')
	"let l:cmd = [g:vimtex_view_general_viewer, '-r']
	"if !empty(system('pgrep Skim'))
		"call extend(l:cmd, ['-g'])
	"endif
	"if has('nvim')
		"call jobstart(l:cmd + [line('.'), l:out, l:tex])
	"elseif has('job')
		"call job_start(l:cmd + [line('.'), l:out, l:tex])
	"else
		"call system(join(l:cmd + [line('.'), shellescape(l:out), shellescape(l:tex)], ' '))
	"endif
"endfunction

"let g:vimtex_quickfix_ignore_all_warnings=1
let g:vimtex_quickfix_mode=0
map ,ll <leader>ll

" ,ls to forward search
"map ,ls :w<CR>:silent !/Applications/Skim.app/Contents/SharedSupport/displayline -r <C-r>=line('.')<CR> %<.pdf %<CR><CR>

" ,lv to display pdf 
map ,lv <leader>lv



if !exists('g:ycm_semantic_triggers')
	let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers.tex = [
			\ 're!\\[A-Za-z]*cite[A-Za-z]*(\[[^]]*\]){0,2}{[^}]*',
			\ 're!\\[A-Za-z]*ref({[^}]*|range{([^,{}]*(}{)?))',
			\ 're!\\hyperref\[[^]]*',
			\ 're!\\includegraphics\*?(\[[^]]*\]){0,2}{[^}]*',
			\ 're!\\(include(only)?|input){[^}]*',
			\ 're!\\\a*(gls|Gls|GLS)(pl)?\a*(\s*\[[^]]*\]){0,2}\s*\{[^}]*',
			\ 're!\\includepdf(\s*\[[^]]*\])?\s*\{[^}]*',
			\ 're!\\includestandalone(\s*\[[^]]*\])?\s*\{[^}]*',
			\ ]



let g:lexical#spell_key = '<leader>s'
let g:lexical#thesaurus_key = '<leader>k'
let g:lexical#dictionary_key = '<leader>d'
let g:tex_flavor='latex'


let g:startify_bookmarks = [ {'v': '~/.vimrc'}, '~/cap/cap' ]

nnoremap <leader>lk :! './doitall'<cr>

let g:voom_latex_sections = ['part', 'chapter', 'section', 'subsection', 'subsubsection', 'paragraph', 'subparagraph', 'outnote']
let g:voom_tree_placement = "right"
let g:voom_tree_width = 120
let g:voom_return_key = "<C-Return>"
let g:voom_tab_key = "<C-Tab>"
let g:voom_default_mode = 'latex'

map <C-n> :NERDTreeToggle<CR>
let NERDTreeIgnore=['__pycache__', '.pyc$[[file]]', '.log$[[file]]']


autocmd FileType typescript JsPreTmpl html
autocmd FileType typescript syn clear foldBraces


set wildignore+=*.swp
set wildignore+=*/build/*


set secure

