if exists("b:current_syntax")
	finish
endif


syntax match confComment "\v#.*$"
highlight link confComment Comment
syntax region confString start=/\v"/ skip=/\v\\./ end=/\v"/
highlight link confString String


syntax match confNumber "\v<\d+>"
syntax match confNumber "\v<\d+\.\d+>"
syntax match confNumber "\v<\d*\.?\d+([Ee]-?)?\d+>"
syntax match confNumber "\v<0x\x+([Pp]-?)?\x+>"
syntax match confNumber "\v<0b[01]+>"
syntax match confNumber "\v<0o\o+>"

highlight link confNumber Number


syntax region confBlock start="{" end="}" fold transparent
